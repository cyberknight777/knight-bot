# Knight-Bot v0.8.0

## A telegram bot made in rust with teloxide framework

> Features list :
+ Sends text that is queried
+ Checks bot is alive or dead
+ Shows neofetch output
+ Get ip addr info
+ Shows what any command is about from man page
+ Sends a why do you ask text
+ Prints currently running processes
+ Gets WHOIS info of a site
+ Gets the last redirected URL
+ Gets definition of word from urban dictionary
+ Adds Sub-Shell Command
+ Sends random text 
+ Generates lucky number
+ Adds Shutdown command
+ Gets UserID & ChatID
+ Start command for first-time PM
* Cat command to send cat pics based on http codes
* Download command to download files
* Upload command to upload files
* Rtfm command to say read the fucking manual
* 

> To Setup & Use :
```bash
cd knight-bot
export TELOXIDE_TOKEN=your_bot_token
edit src/main.rs and add api key from ipinfo.io
then add your bot name there too
then add your userid from telegram in it
cargo run
```
# Why i used rust?

## Rust is a systems programming language & one that is getting mainstream currently

> Thanks To:
+ [konata](https://git.ghnou.su/ghnou/konata)
+ [pero rust bot](https://t.me/pero_rust_bot)
+ [gabu](https://github.com/Nick80835/microbot)

# License

## This bot is released under MIT License. 
